#include "gzstream.h"
#include "misc.h"
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

void generate_file(const std::vector<bool>& keep, const std::string& name,
                   const std::string& prefix)
{
    bool is_gz;
    auto input = misc::load_stream(name, is_gz);
    std::string line;
    int num_line = 0;
    auto out = misc::load_gzostream(prefix + ".hap.gz");
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        if (keep[num_line]) *out << line << "\n";
    }
    input.reset();
    out.reset();
}

std::vector<bool> filter_legend(const std::string& legend_file,
                                const std::string& prefix)
{
    auto input = misc::load_stream(legend_file);
    auto out = misc::load_gzostream(prefix + ".legend.gz");
    std::string line;
    std::vector<std::string> token;
    std::vector<size_t> filter_idx;
    // this is the header
    std::getline(*input, line);
    // we assume the header has EUR EAS and AFR. We hard code for now as we
    // don't have time to make this all purpose

    misc::trim(line);
    if (line.empty())
    {
        throw std::runtime_error(
            "Error: Cannot have empty header line for the legend file");
    }
    misc::split(line, token);
    for (size_t i = 0; i < token.size(); ++i)
    {
        if (token[i] == "EUR" || token[i] == "AFR" || token[i] == "EAS")
            filter_idx.push_back(i);
    }
    std::vector<bool> result;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        // this might be problematic if empty line of hap isn't the same as
        // those in legend
        if (line.empty()) continue;
        misc::split(line, token);
        bool keep = false;
        for (auto&& idx : filter_idx)
        {
            if (token.size() > idx)
            { throw std::runtime_error("Error: Malform legend file"); }
            try
            {
                auto filter = misc::convert<double>(token[idx]);
                if (filter >= 0.01)
                {
                    keep = true;
                    break;
                }
            }
            catch (...)
            {
                throw std::runtime_error("Error: MAF is non-numeric");
            }
        }
        if (keep) { *out << line << "\n"; }
        result.push_back(keep);
    }
    out.reset();
    return result;
}
int main(int argc, char* argv[])
{
    // quick software to split impute haplotypes into per super population
    // haplotypes
    if (argc < 4)
    {
        std::cerr << "Usage: " << argv[0]
                  << " <legend file> <haplotype> <output>" << std::endl;
        return -1;
    }
    const std::string legend_file = argv[1];
    const std::string hap_name = argv[2];
    const std::string output_prefix = argv[3];
    auto keep_lines = filter_legend(legend_file, output_prefix);
    generate_file(keep_lines, hap_name, output_prefix);
    std::cerr << "Completed" << std::endl;
    return 0;
}
