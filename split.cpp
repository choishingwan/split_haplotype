#include "gzstream.h"
#include "misc.h"
#include <fstream>
#include <iostream>
#include <string>
#include <unordered_map>
#include <unordered_set>
#include <vector>

std::tuple<std::vector<int>, std::unordered_map<std::string, int>>
sample_pop(const std::string& name)
{
    // assume header, and third column is the super population, which we want
    bool is_gz;
    auto file = misc::load_stream(name, is_gz);
    std::string line;
    // assume there's a header
    std::vector<int> population;
    std::unordered_map<std::string, int> pop_code;
    std::vector<std::string> token;
    std::getline(*file, line);
    int idx = 0;
    while (std::getline(*file, line))
    {
        misc::trim(line);
        if (line.empty())
        {
            throw std::runtime_error("Error: Don't expect empty line in file");
        }
        misc::split(line, token);
        if (token.size() < 3)
        {
            throw std::runtime_error(
                "Error: Expect at least 3 column for the sample file");
        }
        if (pop_code.find(token[2]) == pop_code.end())
        {
            pop_code[token[2]] = idx;
            idx++;
        }
        population.push_back(pop_code[token[2]]);
    }
    file.reset();
    return {population, pop_code};
}

void generate_files(const std::vector<int>& population,
                    const std::unordered_map<std::string, int>& population_code,
                    const std::vector<bool>& valid, const std::string& name,
                    const std::string& prefix)
{
    auto num_pop = population_code.size();
    // generate output files
    std::vector<std::unique_ptr<std::ostream>> output;
    std::vector<std::string> outnames(num_pop);
    for (auto&& pop : population_code)
    { outnames[pop.second] = prefix + "-" + pop.first + "-hap.gz"; }
    // now open the outputs
    for (size_t i = 0; i < num_pop; ++i)
    { output.push_back(misc::load_gzostream(outnames[i])); }
    bool is_gz;
    auto input = misc::load_stream(name, is_gz);
    std::string line;
    std::vector<std::string> token;
    std::vector<bool> has_tab(output.size());
    int num_line = 0;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        std::fill(has_tab.begin(), has_tab.end(), false);
        if (num_line % 1000 == 0)
        { std::cerr << "Processed " << num_line << " lines\r"; }
        if (!valid[num_line])
        {
            ++num_line;
            continue;
        }
        ++num_line;
        misc::split(line, token);
        for (size_t i = 0; i < token.size(); ++i)
        {
            auto pop_idx = i / 2;
            if (pop_idx >= population.size())
            {
                throw std::runtime_error(
                    "Erorr: mismatch sample and haplotype file");
            }
            auto file_idx = population[pop_idx];
            if (!has_tab[file_idx])
            {
                (*output[file_idx]) << token[i];
                has_tab[file_idx] = true;
            }
            else
                (*output[file_idx]) << "\t" << token[i];
        }
        for (size_t i = 0; i < has_tab.size(); ++i)
        {
            // we should always have write something to the haplotype file if
            // the sample file is correct
            (*output[i]) << "\n";
        }
    }
    std::cerr << "Processed " << num_line << " lines\n";
    input.reset();
    for (auto&& o : output) { o.reset(); }
}

std::unordered_set<std::string> read_hapmap(const std::string& name)
{
    std::unordered_set<std::string> res;
    auto input = misc::load_stream(name);
    std::string line;
    std::vector<std::string> token;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        misc::split(line, token);
        res.insert(token.front());
    }
    input.reset();
    return res;
}
std::vector<bool> filter_legend(const std::unordered_set<std::string>& hapmap,
                                const std::string& legend_file,
                                const std::string& prefix)
{
    bool is_gz;
    auto input = misc::load_stream(legend_file, is_gz);
    auto out = misc::load_gzostream(prefix + ".legend.gz");
    std::string line;
    std::vector<std::string> token, id;
    std::vector<size_t> filter_idx;
    // this is the header
    std::getline(*input, line);
    // we assume the header has EUR EAS and AFR. We hard code for now as we
    // don't have time to make this all purpose

    misc::trim(line);
    if (line.empty())
    {
        throw std::runtime_error(
            "Error: Cannot have empty header line for the legend file");
    }
    misc::split(line, token);
    for (size_t i = 0; i < token.size(); ++i)
    {
        if (token[i] == "EUR" || token[i] == "AFR" || token[i] == "EAS")
            filter_idx.push_back(i);
    }
    *out << line << "\n";
    std::vector<bool> result;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        // this might be problematic if empty line of hap isn't the same as
        // those in legend
        if (line.empty()) continue;
        misc::split(line, token);
        bool keep = false;
        for (auto&& idx : filter_idx)
        {
            if (token.size() <= idx)
            { throw std::runtime_error("Error: Malform legend file"); }
            // check if it is hapmap SNP
            try
            {
                if (token[4] != "Biallelic_SNP") { continue; }
                auto locate = hapmap.find(token[0]);
                if (locate == hapmap.end())
                {
                    // because of format
                    misc::split(token[0], id, ":");
                    locate = hapmap.find(id[0]);
                    if (locate == hapmap.end()) { continue; }
                    token[0] = id[0];
                }
                auto filter = misc::convert<double>(token[idx]);
                if (filter >= 0.01 & filter <= 0.99)
                {
                    keep = true;
                    break;
                }
            }
            catch (...)
            {
                throw std::runtime_error("Error: MAF is non-numeric");
            }
        }
        if (keep)
        {

            *out << token.front();
            for (size_t i = 1; i < token.size(); ++i)
            { *out << "\t" << token[i]; }
            *out << "\n";
        }
        result.push_back(keep);
    }
    out.reset();
    return result;
}
int main(int argc, char* argv[])
{
    // quick software to split impute haplotypes into per super population
    // haplotypes
    if (argc < 6)
    {
        std::cerr
            << "Usage: " << argv[0]
            << " <legend file> <sample file> <haplotype> <hapmap SNPs> <output>"
            << std::endl;
        return -1;
    }
    const std::string legend_file = argv[1];
    const std::string sample_file = argv[2];
    const std::string hap_name = argv[3];
    const std::string hapmap_name = argv[4];
    const std::string output_prefix = argv[5];
    std::cerr << "Read in hap map SNPs" << std::endl;
    auto hapmap = read_hapmap(hapmap_name);
    std::cerr << "Start reading in legend file" << std::endl;
    auto valid = filter_legend(hapmap, legend_file, output_prefix);
    std::cerr << "Start reading population information" << std::endl;
    auto [population, population_code] = sample_pop(sample_file);
    // now output the corresponding files
    std::cerr << "Start working on splitting the haplotype file" << std::endl;
    generate_files(population, population_code, valid, hap_name, output_prefix);
    std::cerr << "Completed" << std::endl;
    return 0;
}
