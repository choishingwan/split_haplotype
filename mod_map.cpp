#include "gzstream.h"
#include "misc.h"
#include <algorithm>
#include <fstream>
#include <math.h>
#include <set>
#include <stdexcept>
#include <string>
#include <thread>
#include <unordered_map>
#include <vector>

std::unordered_map<std::string, std::string>
get_pop_dict(const std::string& file)
{
    auto input = misc::load_stream(file);
    std::string line;
    std::vector<std::string> token;
    std::unordered_map<std::string, std::string> dict;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        misc::split(line, token);
        if (token.size() != 2)
        {
            throw std::runtime_error("Error: Malformed population map file, "
                                     "require only two columns!");
        }
        dict[token[0]] = token[1];
    }
    return dict;
}

std::unordered_map<std::string, std::vector<std::string>>
split_gene_map_to_pop(const std::unordered_map<std::string, std::string>& dict,
                      const std::string& file)
{
    std::unordered_map<std::string, std::vector<std::string>> res;
    auto input = misc::load_stream(file);
    std::string line;
    std::vector<std::string> token;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        misc::split(line, token);
        if (token.size() != 2)
        {
            throw std::runtime_error("Error: Malform genetic map information "
                                     "file. Require only two columns!");
        }
        auto pop = dict.find(token[0]);
        if (pop == dict.end())
        {
            std::cerr
                << "Population: " << token[0]
                << " not included in the population map. It will be ignored"
                << std::endl;
        }
        else
        {
            res[pop->second].push_back(token[1]);
        }
    }
    return res;
}

void merge_genetic_maps(const std::vector<std::string>& input_files,
                        const std::string& prefix)
{
    // files have header
    auto out_file = misc::load_ostream(prefix);
    // set should be ordered
    std::set<int> gp_map;
    bool is_gz;
    std::string line;
    std::vector<std::string> token;
    for (auto&& f : input_files)
    {
        auto input = misc::load_stream(f, is_gz);
        // remove header
        std::getline(*input, line);
        while (std::getline(*input, line))
        {
            misc::trim(line);
            if (line.empty()) continue;
            misc::split(line, token);
            if (token.size() < 3)
            {
                throw std::runtime_error("Error: Malformed genetic map file: "
                                         + f + "."
                                         + "Require at least 3 columns");
            }
            try
            {
                gp_map.insert(misc::convert<int>(token[0]));
            }
            catch (const std::runtime_error&)
            {
                throw std::runtime_error(
                    "Error: non-numeric position number observed: " + token[0]
                    + " in " + f);
            }
        }
        input.reset();
    }
    // we now know all the position number
    // go through each file again'
    std::vector<int> genetic_positions(gp_map.begin(), gp_map.end());
    std::vector<double> rate(genetic_positions.size(), 0.0),
        cM(genetic_positions.size(), 0.0);
    for (auto&& f : input_files)
    {
        auto input = misc::load_stream(f);
        // remove header
        std::getline(*input, line);
        // previous
        double prev_pos = 0, prev_rate = 0, prev_cM = 0;
        for (size_t cur_idx = 0; cur_idx < genetic_positions.size(); ++cur_idx)
        {
            //
        }
    }
}
int main(int argc, char* argv[])
{
    // inputs are population map and genetic map
    if (argc < 4)
    {
        std::cerr << "Usage: " << argv[0]
                  << " <population map file> <genetic map file> <output "
                     "prefix> <thread>"
                  << std::endl;
        return -1;
    }
    try
    {
        const std::string pop_map_name = argv[1];
        const std::string genetic_map_name = argv[2];
        const std::string prefix = argv[3];
        const int thread = (argc > 4) ? misc::convert<int>(argv[4]) : 1;
        auto pop_dict = get_pop_dict(pop_map_name);

        // now map all files into the same super population
        auto pop_map = split_gene_map_to_pop(pop_dict, genetic_map_name);
        // Use multi-thread for each super population.
        const auto use_thread = static_cast<int>(pop_map.size()) > thread
                                    ? thread
                                    : static_cast<int>(pop_map.size());
        auto processed = pop_map.begin();
        std::vector<std::thread> thread_pool;
        while (processed != pop_map.end())
        {
            // do multi-threading
            for (int i = 0; i < use_thread || processed == pop_map.end(); ++i)
            {
                auto genetic_maps = processed->second;
                thread_pool.push_back(std::thread(merge_genetic_maps,
                                                  std::cref(genetic_maps),
                                                  std::cref(prefix)));
                ++processed;
            }
            for (auto&& tr : thread_pool) { tr.join(); }
            thread_pool.clear();
        }
    }
    catch (const std::runtime_error&)
    {
        std::cerr << "Error: Invalid thread number" << std::endl;
    }

    return 0;
}
