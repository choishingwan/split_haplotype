#ifndef MISC_H
#define MISC_H
#include <algorithm>
#include <filesystem>
#include <fstream>
#include <gzstream.h>
#include <math.h>
#include <memory>
#include <sstream>
#include <string>
#include <vector>

namespace misc
{

inline std::vector<std::string> split(const std::string& seq,
                                      const std::string& separators = "\t ")
{
    std::size_t prev = 0, pos;
    std::vector<std::string> result;
    while ((pos = seq.find_first_of(separators, prev)) != std::string::npos)
    {
        if (pos > prev) result.emplace_back(seq.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < seq.length()) result.emplace_back(seq.substr(prev, pos - prev));
    return result;
}
inline void split(const std::string& seq, std::vector<std::string>& res,
                  const std::string& separators = "\t ")
{
    res.clear();
    std::size_t prev = 0, pos;
    while ((pos = seq.find_first_of(separators, prev)) != std::string::npos)
    {
        if (pos > prev) res.emplace_back(seq.substr(prev, pos - prev));
        prev = pos + 1;
    }
    if (prev < seq.length()) res.emplace_back(seq.substr(prev, pos - prev));
}

inline void replace_substring(const std::string& search,
                              const std::string& replace, std::string& s)
{
    if (search.empty()) return;
    for (size_t pos = 0;; pos += replace.length())
    {
        // Locate the substring to replace
        pos = s.find(search, pos);
        if (pos == std::string::npos) break;
        // Replace by erasing and inserting
        s.erase(pos, search.length());
        s.insert(pos, replace);
    }
}

inline void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    [](int ch) { return std::isgraph(ch); }));
}
inline void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         [](int ch) { return std::isgraph(ch); })
                .base(),
            s.end());
}
inline void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
};
inline bool remove_file(const std::string& in)
{
    std::filesystem::path f {in};
    return std::filesystem::remove(f);
}
inline bool file_exists(const std::string& file)
{
    std::filesystem::path f {file};
    return std::filesystem::exists(f);
}

class Convertor
{
public:
    template <typename T>
    static T convert(const std::string& str)
    {
        errno = 0;
        std::istringstream iss(str);
        T obj;
        iss >> obj;
        if (!iss.eof() || iss.fail())
        {
            if constexpr (std::is_same_v<T, double>)
            {
                throw std::runtime_error(
                    "Unable to convert the input. It is possible that your "
                    "number is very close to 0, e.g. smaller than "
                    + std::to_string(std::numeric_limits<double>::epsilon()));
            }
            else
                throw std::runtime_error("Unable to convert the input");
        }

        if constexpr (std::is_same_v<T, size_t>)
        {
            // easiest way to check for negative
            if (str.at(0) == '-')
            {
                throw std::runtime_error("Error: Negative input for a positive "
                                         "variable");
            }
        }
        return obj;
    }

private:
    static std::istringstream iss;
};
// wrapper for to make things easier
template <typename T>
inline T convert(const std::string& str)
{
    return Convertor::convert<T>(str);
}

inline bool isNumeric(const std::string& s)
{
    try
    {
        convert<double>(s);
    }
    catch (...)
    {
        return false;
    }
    return true;
}


inline size_t get_num_line(std::unique_ptr<std::istream>& input)
{
    // assert(input->is_open());
    size_t num_line = 0;
    std::string line;
    while (std::getline(*input, line))
    {
        misc::trim(line);
        if (line.empty()) continue;
        ++num_line;
    }
    input->clear();
    input->seekg(0, input->beg);
    return num_line;
}

inline bool logically_equal(double a, double b, double error_factor = 1.0)
{
    return ((a == b)
            || (std::abs(a - b) < std::abs(std::min(a, b))
                                      * std::numeric_limits<double>::epsilon()
                                      * error_factor));
}

inline std::unique_ptr<std::istream>
load_stream(const std::string& filepath,
            std::ios_base::openmode mode = std::ios_base::in)
{
    if (filepath.empty())
        throw std::runtime_error("Error: Cannot open file with empty name");
    auto file = std::make_unique<std::ifstream>(filepath.c_str(), mode);
    if (!file->is_open())
    { throw std::runtime_error("Error: Cannot open file: " + filepath); }
    return std::unique_ptr<std::istream>(*file ? std::move(file) : nullptr);
}
inline std::unique_ptr<std::ostream>
load_ostream(const std::string& filepath,
             std::ios_base::openmode mode = std::ios_base::out)
{
    auto file = std::make_unique<std::ofstream>(filepath.c_str(), mode);
    if (!file->is_open())
    { throw std::runtime_error("Error: Cannot open file: " + filepath); }
    return std::unique_ptr<std::ostream>(*file ? std::move(file) : nullptr);
}
inline std::unique_ptr<std::ostream> load_gzostream(const std::string& filepath)
{
    auto file =
        std::make_unique<GZSTREAM_NAMESPACE::ogzstream>(filepath.c_str());
    if (!file->good())
    { throw std::runtime_error("Error: Cannot open file: " + filepath); }
    return std::unique_ptr<std::ostream>(*file ? std::move(file) : nullptr);
}
inline bool is_gz_file(const std::string& name)
{
    const unsigned char gz_magic[2] = {0x1f, 0x8b};
    FILE* fp;
    if ((fp = fopen(name.c_str(), "rb")) == nullptr)
    { throw std::runtime_error("Error: Cannot open file - " + name); }
    unsigned char buf[2];
    if (fread(buf, 1, 2, fp) == 2)
    {
        if (buf[0] == gz_magic[0] && buf[1] == gz_magic[1]) { return true; }
        return false;
    }
    else
    {
        // can open the file, but can't read the magic number.
        return false;
    }
}

inline std::unique_ptr<std::istream> load_stream(const std::string& filepath,
                                                 bool& gz_input)
{
    gz_input = false;
    try
    {
        gz_input = misc::is_gz_file(filepath);
    }
    catch (const std::runtime_error& e)
    {
        throw std::runtime_error(e.what());
    }
    if (gz_input)
    {
        auto gz =
            std::make_unique<GZSTREAM_NAMESPACE::igzstream>(filepath.c_str());
        if (!gz->good())
        {
            throw std::runtime_error("Error: Cannot open file: " + filepath
                                     + " (gz) to read!\n");
        }
        return std::unique_ptr<std::istream>(*gz ? std::move(gz) : nullptr);
    }
    else
    {
        return load_stream(filepath);
    }
}

// NOTE: Didn't work for non-ASCII characters
inline void to_upper(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::toupper);
}
inline void to_lower(std::string& str)
{
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
}
// from https://stackoverflow.com/a/874160
inline bool hasEnding(const std::string& fullString, const std::string& ending)
{
    if (fullString.empty())
        throw std::runtime_error(
            "Error: Cannot look for ending of an empty string");
    else if (ending.empty())
        throw std::runtime_error(
            "Error: Undefined behaviour. Cannot look for empty ending in "
            "string");
    else if (fullString.length() >= ending.length())
    {
        return (fullString.compare(fullString.length() - ending.length(),
                                   ending.length(), ending)
                == 0);
    }
    else
    {
        return false;
    }
}
}


#endif // MISC_H
